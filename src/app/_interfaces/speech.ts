export interface Speech {
   message: string;
   date: string;
   author: string;
   subject: string;
   key: string;
}
