import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
   name: 'mySearchInfo'
})
export class SearchInfoPipe implements PipeTransform {

   public transform(arrayToSearch: any[], searchTerm: string): any {
      if (!searchTerm) { return arrayToSearch; }
      searchTerm = searchTerm.toLowerCase();

      return arrayToSearch.filter((item: any) => {
         if ((item.name).toLowerCase().indexOf(searchTerm) > -1) {
            return true;
         } else {
            return false;
         }
      });
   }

}
