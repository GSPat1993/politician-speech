import { Component, Input } from '@angular/core';
import { AbstractControlDirective, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-validate-error',
  templateUrl: './validate-error.component.html',
  styleUrls: ['./validate-error.component.scss']
})
export class ValidateErrorComponent {
  private static readonly errorMessages = {
    'email': () => 'Invalid Email Format',
    'required': () => 'This field is required'
  };

  @Input() private control: AbstractControlDirective | AbstractControl;

  public shouldShowErrors(): boolean {
    return this.control &&
      this.control.errors &&
      (this.control.dirty || this.control.touched);
  }

  public listOfErrors(): string[] {
    return Object.keys(this.control.errors)
      .map(field => this.getMessage(field, this.control.errors[field]));
  }

  private getMessage(type: string, params: any) {
    return ValidateErrorComponent.errorMessages[type](params);
  }
}
