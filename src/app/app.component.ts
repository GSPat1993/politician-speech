import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { AuthService } from './_providers/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private router: Router,
    private authServ: AuthService
  ) { }

  public ngOnInit() {
    this.redirectPage(this.authServ.getAuthToken);
    this.authServ.isLoggedIn().subscribe((data) => {
      this.redirectPage(data);
    });
  }

  public redirectPage(auth) {
    if (auth) {
      this.router.navigateByUrl('/account');
    } else {
      this.router.navigateByUrl('/login');
    }
  }
}
