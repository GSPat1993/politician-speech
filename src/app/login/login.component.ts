import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { AuthService } from '../_providers/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public error = {
    code: '',
    message: ''
  };
  public loading = false;
  public loginForm: FormGroup = this.builder.group({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required])
  });

  constructor(
    private renderer: Renderer2,
    private builder: FormBuilder,
    private authServ: AuthService
  ) { }

  public ngOnInit() {
    this.renderer.setStyle(document.body, 'background', '#FFFFFF');
    this.renderer.setStyle(document.body, 'padding-top', '90px');
  }

  public login(creds = this.loginForm.value) {
    this.loading = true;
    this.authServ.login(creds.email, creds.password).then((result) => {
      this.authServ.setAuthToken(result).then(() => {
        this.loading = false;
        this.loginForm.reset();
      });
    }).catch((error) => {
      this.loading = false;
      this.error = error;
    });
  }

}
