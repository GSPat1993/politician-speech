import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

import { Subject, Observable } from 'rxjs';

@Injectable()
export class AuthService {

  public logger = new Subject<any>();
  constructor(
    private afAuth: AngularFireAuth
  ) { }

  public isLoggedIn(): Observable<any> {
    return this.logger.asObservable();
  }

  public login(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  public get getAuthToken() {
    return localStorage.getItem('politician-speech.refreshToken');
  }

  public get getAuthUser() {
    if (localStorage.getItem('politician-speech.uid') && localStorage.getItem('politician-speech.email')) {
      return { uid: localStorage.getItem('politician-speech.uid'), email: localStorage.getItem('politician-speech.email') };
    } else {
      return null;
    }
  }

  public setAuthToken(auth) {
    return new Promise((resolve) => {
      localStorage.setItem('politician-speech.uid', auth.user.uid);
      localStorage.setItem('politician-speech.email', auth.user.email);
      localStorage.setItem('politician-speech.refreshToken', auth.user.refreshToken);
      this.logger.next(true);
      resolve(true);
    });
  }

  public removeAuth() {
    return new Promise((resolve) => {
      localStorage.clear();
      this.logger.next(false);
      resolve(true);
    });
  }

}
