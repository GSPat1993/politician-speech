import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';

import { Observable } from 'rxjs';

@Injectable()
export class SpeechService {

   constructor(
      private db: AngularFireDatabase,
      private afAuth: AngularFireAuth
   ) { }

   public setSpeech(data, uid) {
      return this.db.database.ref('/speeches').child(uid).push(data);
   }

   public removeSpeech(uid, key) {
      return this.db.database.ref(`/speeches/${uid}/${key}`).remove();
   }

   public updateSpeech(uid, data, key) {
      return this.db.database.ref(`/speeches/${uid}/${key}`).update(data);
   }

   public getSpeeches(uid): Observable<any> {
      return this.db.object(`/speeches/${uid}`).valueChanges();
   }

   public getAllSpeeches(): Observable<any> {
      return this.db.object(`/speeches`).valueChanges();
   }
}
