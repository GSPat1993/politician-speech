import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireDatabaseModule } from 'angularfire2/database';

// Providers
import { AuthService } from './_providers/auth.service';
import { SpeechService } from './_providers/speech.service';

// Components
import { LoginComponent } from './login/login.component';
import { ValidateErrorComponent } from './validate-error/validate-error.component';

export const firebaseConfig = {
  projectId: 'politician-speech',
  messagingSenderId: '326700431325',
  storageBucket: 'politician-speech.appspot.com',
  authDomain: 'politician-speech.firebaseapp.com',
  apiKey: 'AIzaSyByp8cbdx_VV1ZtRtUtn3EzRU8Xu6ExsNM',
  databaseURL: 'https://politician-speech.firebaseio.com'
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ValidateErrorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,

    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),

    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    AuthService,
    SpeechService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
