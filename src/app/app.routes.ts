import { Routes, CanActivate } from '@angular/router';

import { LoginComponent } from './login/login.component';

export const ROUTES: Routes = [
   { path: 'login', component: LoginComponent },
   { path: 'account', loadChildren: './+account/account.module#AccountModule' }
];
