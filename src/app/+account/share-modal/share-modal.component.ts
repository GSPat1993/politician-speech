import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap/modal';
import { timeout } from 'q';

@Component({
  selector: 'app-share-modal',
  templateUrl: './share-modal.component.html',
  styleUrls: ['./share-modal.component.scss']
})
export class ShareModalComponent {
  @ViewChild('shareModal') public shareModal: ModalDirective;

  public shared = false;
  public loading = false;
  public shareForm: FormGroup = this.builder.group({
    email: new FormControl('', [Validators.required, Validators.email])
  });

  constructor(
    private builder: FormBuilder
  ) { }

  public showModal(modal?) {
    if (modal) {
      this[modal].show();
    }
  }

  public hideModal(modal?) {
    this.shared = false;
    if (modal) {
      this[modal].hide();
    }
  }

  public share() {
    this.loading = true;
    setTimeout(() => {
      this.shared = true;
      this.loading = false;
      this.shareForm.reset();
    }, 3000);
  }
}
