import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { AuthService } from '../../_providers/auth.service';
import { SpeechService } from '../../_providers/speech.service';

@Component({
  selector: 'app-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.scss']
})
export class SubmitComponent {

  public success = {
    code: false,
    message: ''
  };
  public loading = false;
  public speechForm: FormGroup = this.builder.group({
    message: new FormControl('', [Validators.required]),
    subject: new FormControl('', [Validators.required]),
    author: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
  });

  constructor(
    private builder: FormBuilder,
    private authServ: AuthService,
    private speechServ: SpeechService
  ) { }

  public submit(form = this.speechForm.value) {
    const authServ = this.authServ.getAuthUser;

    this.loading = true;
    this.speechServ.setSpeech(form, authServ.uid).then((result) => {
      this.loading = false;
      this.speechForm.reset();
      this.success = {
        code: true,
        message: 'Added Speech Successfully!'
      };
    });
  }
}
