import { Component, OnInit, Renderer2 } from '@angular/core';
import { Location } from '@angular/common';

import { AuthService } from '../_providers/auth.service';
@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public isCollapsed = false;
  constructor(
    public location: Location,
    private renderer: Renderer2,
    private authServ: AuthService
  ) { }

  public ngOnInit() {
    this.renderer.setStyle(document.body, 'background', ' #d4d4d4');
    this.renderer.setStyle(document.body, 'padding-top', 'unset');

  }

  public logout() {
    this.authServ.removeAuth();
  }

}
