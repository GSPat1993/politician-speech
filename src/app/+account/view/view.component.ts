import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { AuthService } from '../../_providers/auth.service';
import { SpeechService } from '../../_providers/speech.service';

import { Auth } from '../../_interfaces/auth';
import { Speech } from '../../_interfaces/speech';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  @ViewChild('shareModal') public shareModal;

  public noData = false;
  public loading = {
    delete: false,
    update: false,
    data: false
  };
  public status = {
    code: false,
    message: ''
  };
  public speeches: any[];
  public speechSelected: Speech;
  public speechForm: FormGroup = this.builder.group({
    message: new FormControl('', [Validators.required]),
    subject: new FormControl('', [Validators.required]),
    author: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
  });

  private authUser: Auth;

  constructor(
    private renderer: Renderer2,
    private builder: FormBuilder,
    private authServ: AuthService,
    private speechServ: SpeechService
  ) { }

  public ngOnInit() {
    this.authUser = this.authServ.getAuthUser;
    this.getSpeeches();
  }

  public selectItem(speech) {
    this.speechSelected = speech;
    for (const key in speech) {
      if (this.speechForm.value.hasOwnProperty(key)) {
        this.speechForm.controls[key].setValue(speech[key], { onlySelf: true });
      }
    }
  }

  public delete(speech: Speech = this.speechSelected) {
    this.loading.delete = true;
    this.speechServ.removeSpeech(this.authUser.uid, speech.key).then((removed) => {
      this.speechForm.reset();
      this.loading.delete = false;
      this.speechSelected = null;
      this.status = {
        code: true,
        message: 'Deleted Successfully'
      };
    });
  }

  public update(speech: Speech = this.speechForm.value) {
    const key = this.speechSelected.key;

    this.loading.update = true;
    this.speechServ.updateSpeech(this.authUser.uid, speech, key).then((updated) => {
      this.speechSelected = null;
      this.loading.update = false;
      this.status = {
        code: true,
        message: 'Updated Successfully'
      };
    });
  }

  public share() {
    this.shareModal.showModal('shareModal');
  }

  private getSpeeches() {
    const arrayContainer = [];

    this.speechServ.getSpeeches(this.authUser.uid).subscribe((result) => {
      this.loading.data = true;
      if (result) {
        const keys = Object.keys(result);
        const values = Object.values(result);

        this.noData = false;
        values.map((val, key) => {
          val['key'] = keys[key];
          return val;
        });
        this.speeches = values;
      } else {
        this.noData = true;
      }
    }, (error) => {
      console.log(error);
    });
  }

}
