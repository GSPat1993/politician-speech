import { Component, OnInit } from '@angular/core';

import { SpeechService } from '../../_providers/speech.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  public noData = false;
  public search: string;
  public loading = false;
  public users = [
    {
      uid: 'JqueYOc5rjSPN94L7WVABwPTuHk2',
      name: 'Politician One'
    },
    {
      uid: 'Ojkc8078ZQPP0rE0vXT8r8YqyzY2',
      name: 'Politician Two'
    },
    {
      uid: 'kF4A1jg6DrdIRsztS2b9lZva3cz2',
      name: 'Politician Three'
    }
  ];

  constructor(
    private speechServ: SpeechService
  ) { }

  public ngOnInit() {
    this.getSpeeches();
  }

  private getSpeeches() {
    const arrayContainer = [];

    this.speechServ.getAllSpeeches().subscribe((result) => {
      this.loading = true;
      if (result) {
        this.noData = false;
        this.users.map((value) => {
          if (result[value.uid]) {
            value['data'] = Object.values(result[value.uid]);
          }
          return value;
        });
      } else {
        this.noData = true;
      }
    }, (error) => {
      console.log(error);
    });
  }

}
