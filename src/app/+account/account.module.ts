import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ROUTES } from './account.routes';

import { AccountComponent } from './account.component';

import { ViewComponent } from './view/view.component';

// Bootstrap
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { SubmitComponent } from './submit/submit.component';
import { SearchComponent } from './search/search.component';
import { ShareModalComponent } from './share-modal/share-modal.component';

// Pipes
import { SearchInfoPipe } from './../_pipes/search-pipe';

@NgModule({
   imports: [
      FormsModule,
      CommonModule,
      ReactiveFormsModule,

      ModalModule.forRoot(),
      CollapseModule.forRoot(),
      RouterModule.forChild(ROUTES)
   ],
   declarations: [
      ViewComponent,
      SearchComponent,
      SubmitComponent,
      AccountComponent,
      ShareModalComponent,

      SearchInfoPipe
   ],
   bootstrap: [AccountComponent]
})
export class AccountModule { }
