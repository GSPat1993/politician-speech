import { Routes } from '@angular/router';

import { ViewComponent } from './view/view.component';
import { SubmitComponent } from './submit/submit.component';
import { SearchComponent } from './search/search.component';

import { AccountComponent } from './account.component';

export const ROUTES: Routes = [
   {
      path: '', component: AccountComponent, children: [
         { path: '', component: ViewComponent },
         { path: 'view', component: ViewComponent },
         { path: 'submit', component: SubmitComponent },
         { path: 'search', component: SearchComponent },
      ]
   }
];
